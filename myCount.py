# -*-coding: utf-8 -*-
"""
# @Time:2021-10-28
# @Author:wubangying
# @Description:统计重复的资产编号，将资产编号放入dataFile中（换行符间隔开），执行后生成result统计每个资产编号出现次数，
                redundant记录有冗余的资产编号及其冗余次数
"""
fileName = 'D:\code\\redundantcount\dataFile'
data_Lst = []
data_File = open(fileName, "r")
for line in data_File.readlines():
    data_Lst.append(line.strip())

data = list(set(data_Lst))
data_Dic = {}
for item in data:
    data_Dic[item] = 0

for item in data_Lst:
    if item in data_Dic.keys():
        data_Dic[item] += 1
    else:
        pass

newFile = 'result'
new_File = open(newFile, 'w')
redundantFile = 'redundant'
redund_File = open(redundantFile,'w')

for key in data_Dic:
    mystr = key+'\t'+ str(data_Dic[key])+'\n'
    new_File.write(mystr)
    if data_Dic[key] > 2:
        print(key+'\n')
        redund_File.write(key+'\t'+ str(data_Dic[key])+'\n')
new_File.close()
redund_File.close()